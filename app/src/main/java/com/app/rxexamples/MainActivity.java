package com.app.rxexamples;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func0;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;

public class MainActivity extends AppCompatActivity {

    private String value;
    private PublishSubject<String> searchPublisher;

    private MyAsyncTask myAsyncTask;
    private static String TAG_DATA = "TAG_DATA";
    private TextView tvCountNumbers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        testThreadAndHandler();

        testAsyncTask();

        testCreateObservable();

        testObservableFrom();

        compareJustAndDefer();

        testDebounce();

        compareMapFunctions();
    }

    private void testThreadAndHandler() {
        tvCountNumbers = findViewById(R.id.activity_main_tv_count_number);
        Handler countHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                tvCountNumbers.setText("Number: " + msg.arg1);

                String doneMessage = msg.getData().getString(TAG_DATA);
                if (doneMessage != null) {
                    tvCountNumbers.setText(doneMessage);
                }
            }
        };

        Thread countThread = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 1; i < 10; i++) {
                    Message message = new Message();
                    message.arg1 = i;
                    countHandler.sendMessage(message);

                    SystemClock.sleep(1000);
                }

                Message message = new Message();
                Bundle bundle = new Bundle();
                bundle.putString(TAG_DATA, "Done counting!");
                message.setData(bundle);
                countHandler.sendMessage(message);
            }
        });
        countThread.start();
    }

    private void testAsyncTask() {
        myAsyncTask = new MyAsyncTask();
        myAsyncTask.execute(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

    }

    private void testCreateObservable() {
        Observable.create(new Observable.OnSubscribe<Object>() {
            @Override
            public void call(Subscriber<? super Object> subscriber) {
                subscriber.onNext("Cool");
                subscriber.onCompleted();
            }
        }).subscribe(new Observer<Object>() {
            @Override
            public void onCompleted() {
                Log.d("RxTest", "Observable.create: onCompleted");
            }

            @Override
            public void onError(Throwable throwable) {
                Log.d("RxTest", "Observable.create: onError " + throwable.getMessage());
            }

            @Override
            public void onNext(Object o) {
                Log.d("RxTest", "Observable.create: onNext " + o);
            }
        });
    }

    private void testObservableFrom() {
        Integer a[] = new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        Observable.from(a)
                .map(integer -> integer * 3)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(integer -> {
                    Log.d("RxTest", "Observable.from: " + integer);
                }, throwable -> {
                    Log.d("RxTest", "Observable.from: " + throwable.getMessage());
                });


    }

    class HandleNetwork implements Observable.Transformer<String, String> {

        @Override
        public Observable<String> call(Observable<String> stringObservable) {
            stringObservable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());
            return stringObservable;
        }
    }

    private void compareJustAndDefer() {
        Observable<String> justObservable =  Observable.just(value);

        Observable<String> deferObservable =  Observable.defer(new Func0<Observable<String>>() {
            @Override
            public Observable<String> call() {
                return Observable.just(value);
            }
        });

        value = "Good job!";

        justObservable.subscribe(new Action1<String>() {
            @Override
            public void call(String s) {
                Log.d("RxTest", "Just value: " + s);
            }
        });

        deferObservable.subscribe(new Action1<String>() {
            @Override
            public void call(String s) {
                Log.d("RxTest", "Defer value: " + s);
            }
        });
    }

    private void testDebounce() {
        searchPublisher = PublishSubject.create();
        searchPublisher.debounce(500, TimeUnit.MILLISECONDS)
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String input) {
                        Log.d("RxTest", "Call Search API: " + input);
                    }
                });

        EditText etSearch = findViewById(R.id.activity_main_et_search);
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                searchPublisher.onNext(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    private void compareMapFunctions() {
        final String[] items = new String[]{"a", "b", "c", "d", "e", "f"};

        /*Log.d("RxTest", "----------Flat map----------");
        Observable.from(items)
                .flatMap(new Func1<String, Observable<String>>() {
                    @Override
                    public Observable<String> call(String s) {
                        final int delay = new Random().nextInt(3);
                        return Observable.just(s + "x")
                                .delay(delay, TimeUnit.SECONDS);
                    }
                })
                .toList()
                .subscribe(new Action1<List<String>>() {
                    @Override
                    public void call(List<String> values) {
                        for (String v: values) {
                            Log.d("RxTest", v);
                        }
                    }
                });*/
        /*Log.d("RxTest", "----------Concat map----------");
        Observable.from(items)
                .concatMap(new Func1<String, Observable<String>>() {
                    @Override
                    public Observable<String> call(String s) {
                        final int delay = new Random().nextInt(3);
                        return Observable.just(s + "x")
                                .delay(delay, TimeUnit.SECONDS);
                    }
                })
                .toList()
                .subscribe(new Action1<List<String>>() {
                    @Override
                    public void call(List<String> values) {
                        for (String v: values) {
                            Log.d("RxTest", v);
                        }
                    }
                });*/

        Log.d("RxTest", "----------Switch map----------");
        Observable.from(items)
                .switchMap(new Func1<String, Observable<String>>() {
                    @Override
                    public Observable<String> call(String s) {
                        final int delay = new Random().nextInt(2);
                        return Observable.just(s + "x")
                                .delay(delay, TimeUnit.SECONDS);
                    }
                })
                .toList()
                .subscribe(new Action1<List<String>>() {
                    @Override
                    public void call(List<String> values) {
                        for (String v: values) {
                            Log.d("RxTest",  v);
                        }
                    }
                });
    }

    static class MyAsyncTask extends AsyncTask<Integer, Float, String> {

        @Override
        protected void onPreExecute() {
            Log.d("TestAsyncTask", "onPreExecute");
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Integer... integers) {
            Log.d("TestAsyncTask", "doInBackground");
            for (Integer integer : integers) {
                if (!isCancelled()) {
                    publishProgress(Float.valueOf(integer));
                    SystemClock.sleep(2000);
                } else {
                    break;
                }
            }
            return "Finished";
        }

        @Override
        protected void onProgressUpdate(Float... values) {
            super.onProgressUpdate(values);
            Log.d("TestAsyncTask", "onProgressUpdate: " + values[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d("TestAsyncTask", "onPostExecute: " + s);

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        myAsyncTask.cancel(true);
    }
}
